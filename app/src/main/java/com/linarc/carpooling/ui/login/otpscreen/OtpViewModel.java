package com.linarc.carpooling.ui.login.otpscreen;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.text.Html;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.auth.User;
import com.linarc.carpooling.CarpoolingApplication;

import org.joda.time.DateTime;

import java.util.concurrent.TimeUnit;

public class OtpViewModel extends AndroidViewModel {
    Application application;
    FirebaseUser mUser;
    FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private String verificationId;
    String phonenumber;
    public MutableLiveData<Boolean> isdisableResend;
    public MutableLiveData<Boolean> isshowTimer;
    public ObservableField<String> etCode1;
    public ObservableField<String> etCode2;
    public ObservableField<String> etCode3;
    public ObservableField<String> etCode4;
    public ObservableField<String> etCode5;
    public ObservableField<String> etCode6;
    public ObservableField<String> txtTimer;
    public ObservableField<String> tv_setMobileNumber;
    public MutableLiveData<String> onShowErrortxt;
    public MutableLiveData<String> onverifyphonenumber;
    public MutableLiveData<String> mutableverifycode;
    boolean isverified = false;
    public MutableLiveData<Boolean> onNextScreen;

    public MutableLiveData<Boolean> progressdialog;
    private MutableLiveData<String> toastMessageObserver = new MutableLiveData();

    public OtpViewModel(@NonNull Application application) {
        super(application);
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();
        isdisableResend = new MutableLiveData<>();
        isshowTimer = new MutableLiveData<>();
        etCode1 = new ObservableField<String>();
        etCode2 = new ObservableField<String>();
        etCode3 = new ObservableField<String>();
        etCode4 = new ObservableField<String>();
        etCode5 = new ObservableField<String>();
        etCode6 = new ObservableField<String>();
        txtTimer = new ObservableField<String>();
        onShowErrortxt = new MutableLiveData<String>();
        tv_setMobileNumber = new ObservableField<>();
        onverifyphonenumber = new MutableLiveData<>();
        toastMessageObserver = new MutableLiveData<>();
        progressdialog = new MutableLiveData<>();
        mutableverifycode = new MutableLiveData<>();
        onNextScreen = new MutableLiveData<>();

    }

    public void onInitialiseFirebase() {
        isdisableResend.setValue(true);
        isshowTimer.setValue(true);


        new CountDownTimer(150000, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {

                txtTimer.set("" + String.format("%d : %d ",
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
                isdisableResend.setValue(false);
                isshowTimer.setValue(false);

            }
        }.start();


        mAuth = FirebaseAuth.getInstance();
        SharedPreferences mPrefs = getApplication().getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        String number = mPrefs.getString("number", "");
        phonenumber = "+91" + number;
        String setTitle = "Enter the OTP sent to ";
        String phone = "<font color='#000'>" + phonenumber + "</font>";
        tv_setMobileNumber.set(setTitle + Html.fromHtml(phone));
          onverifyphonenumber.setValue(phonenumber);


    }





    public void onResendOtp() {

        onInitialiseFirebase();


    }


    public void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        isverified = false;
                        if (task.isSuccessful()) {
                            getUid();
                            onNextScreen.postValue(true);
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                onShowErrortxt.setValue(e.getMessage());

            }
        });
    }

    public void getUid() {

        UserModel userModel = new UserModel(FirebaseAuth.getInstance().getUid(),"mobile",DateTime.now().toString(),true);


        db.collection("users").document(FirebaseAuth.getInstance().getUid()).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (!documentSnapshot.exists())
                {
                    addDataToFirestore(userModel);

                } else {
                   toastMessageObserver.postValue("Already Register, Please Login");
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                onShowErrortxt.setValue("Verification Failed");

            }
        });


    }

    private void addDataToFirestore(UserModel userModel) {


        CollectionReference dbCourses = db.collection("users");



        // below method is use to add data to Firebase Firestore.
        dbCourses.add(userModel).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
            @Override
            public void onSuccess(DocumentReference documentReference) {
            toastMessageObserver.postValue("Register Successfully");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                toastMessageObserver.postValue("Register failed");
            }
        });
    }

}
