package com.linarc.carpooling.ui.utils.base;


public interface IRunEventCallBack {
    void start();
}
