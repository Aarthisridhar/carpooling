package com.linarc.carpooling.ui.home.rider;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linarc.carpooling.databinding.ActivityRiderBinding;
import com.linarc.carpooling.ui.home.rider.model.RidesModel;
import com.linarc.carpooling.ui.utils.base.BaseActivity;

import java.util.ArrayList;

public class RiderDashboardActivity extends BaseActivity {
    ActivityRiderBinding riderBinding;
    RiderViewModel riderViewModel;
    RiderAdapter riderAdapter;
    FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        riderBinding = ActivityRiderBinding.inflate(getLayoutInflater());
        riderViewModel = new ViewModelProvider(this).get(RiderViewModel.class);
        setContentView(riderBinding.getRoot());
        riderAdapter = new RiderAdapter(RiderDashboardActivity.this, new ArrayList<RidesModel>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RiderDashboardActivity.this);
        riderBinding.deviceRecyclerView.setLayoutManager(mLayoutManager);
        riderBinding.deviceRecyclerView.setItemAnimator(new DefaultItemAnimator());
        riderBinding.deviceRecyclerView.setAdapter(riderAdapter);
        riderViewModel.onGetRidesDetails();
        db = FirebaseFirestore.getInstance();


//        riderViewModel.getMutablerideslist().observe(this, new Observer<ArrayList<RidesModel>>() {
//            @Override
//            public void onChanged(ArrayList<RidesModel> ridesModels) {
//                if(ridesModels.size()>0)
//                {
//                    riderAdapter.reinitialze(ridesModels);                }
//            }
//        });
        listenToAvailableOwners();
    }

    private void listenToAvailableOwners(){
        db.collection("rides")
            .whereEqualTo("status", "ST")
            .addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot value,
                                    @Nullable FirebaseFirestoreException e) {
                    if (e != null) {
                        return;
                    }

                    ArrayList<RidesModel> owners = new ArrayList<>();

                    for (QueryDocumentSnapshot doc : value) {
                        RidesModel owner = new RidesModel();
                        String userFullName = doc.getData().get("userFullName").toString();
                        String price = doc.getData().get("price").toString();
                        String carName = doc.getData().get("carName").toString();
                        String endLocation = doc.getData().get("endLocation").toString();
                        String status = doc.getData().get("status").toString();
                        String tripId = doc.getId();

                        owner.setFullName(userFullName);
                        owner.setPrice(price);
                        owner.setCarName(carName);
                        owner.setEndLocation(endLocation);
                        owner.setTripId(tripId);

                        if(status.equals("ST")){
                            owners.add(owner);
                        }
                    }

                    riderAdapter.reinitialze(owners, db);
                }
            });
    }
}