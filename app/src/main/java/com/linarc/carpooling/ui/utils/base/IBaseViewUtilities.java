package com.linarc.carpooling.ui.utils.base;


import android.view.View;

public interface IBaseViewUtilities {
    void setStatusBarColor(int resId);

    void showProgressDialog(String msg);

    void dismissProgressDialog();

    void dismissOnResume();

    void showMessage(MessageType type, MessagePayload payload, IDialogCallBack callBack);

    void runOnUIThread(IRunEventCallBack callBack);

    void disablePrimaryButton();

    void enablePrimaryButton();

    boolean InternetConnection(View view);



}
