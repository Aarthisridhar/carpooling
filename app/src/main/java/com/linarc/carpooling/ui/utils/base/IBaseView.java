package com.linarc.carpooling.ui.utils.base;


import androidx.annotation.NonNull;

public interface IBaseView<T> {

    void setPresenter(@NonNull T presenter);
}
