package com.linarc.carpooling.ui.home.owner;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linarc.carpooling.ui.home.rider.model.RidesModel;

import java.util.ArrayList;

public class OwnerViewModel extends AndroidViewModel {
    ArrayList<RidesModel> rideslist = new ArrayList<>();
    MutableLiveData<ArrayList<RidesModel>> mutablerideslist;
    FirebaseAuth auth;
    FirebaseUser user;
    FirebaseFirestore db;
    RidesModel ridesModel;

    public OwnerViewModel(@NonNull Application application) {
        super(application);

        db = FirebaseFirestore.getInstance();
        mutablerideslist = new MutableLiveData<ArrayList<RidesModel>>();

    }
    public MutableLiveData<ArrayList<RidesModel>> getMutablerideslist() {
        return mutablerideslist;
    }



    public void onGetRidesDetails() {

        db.collection("rides").whereEqualTo("userId", FirebaseAuth.getInstance().getUid()).addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {


                if (e != null) {
                    return;
                }

                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {

                    ridesModel = doc.toObject(RidesModel.class);

                    rideslist.add(ridesModel);


                }

                mutablerideslist.postValue(rideslist);
            }


        });

    }
}
