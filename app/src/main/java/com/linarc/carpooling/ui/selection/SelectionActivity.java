package com.linarc.carpooling.ui.selection;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.linarc.carpooling.R;
import com.linarc.carpooling.databinding.ActivitySelectionBinding;
import com.linarc.carpooling.ui.home.owner.OwnerDashboardActivity;
import com.linarc.carpooling.ui.home.rider.RiderDashboardActivity;
import com.linarc.carpooling.ui.utils.base.BaseActivity;

public class SelectionActivity extends BaseActivity {
    boolean isOwner = false;
    boolean isRider = false;

    ActivitySelectionBinding activitySelectionBinding;
    SelectionViewModel selectionViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySelectionBinding = ActivitySelectionBinding.inflate(getLayoutInflater());
        selectionViewModel = new ViewModelProvider(this).get(SelectionViewModel.class);
        setContentView(activitySelectionBinding.getRoot());
        activitySelectionBinding.setViewmodel(selectionViewModel);


        activitySelectionBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        activitySelectionBinding.rlCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isOwner = true;
                isRider = false;
                activitySelectionBinding.rlCar.setBackground(getResources().getDrawable(R.drawable.rec_blue));
                activitySelectionBinding.rlRider.setBackground(getResources().getDrawable(R.drawable.rec_grey));

            }
        });

        activitySelectionBinding.rlRider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRider = true;
                isOwner = false;
                activitySelectionBinding.rlRider.setBackground(getResources().getDrawable(R.drawable.rec_blue));
                activitySelectionBinding.rlCar.setBackground(getResources().getDrawable(R.drawable.rec_grey));

            }
        });
        activitySelectionBinding.btnSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isRider && !isOwner) {
                    Toast.makeText(SelectionActivity.this, "Please Select Account type", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isRider) {
                    Intent mainIntent = new Intent(SelectionActivity.this, RiderDashboardActivity.class);
                    startActivity(mainIntent);
                } else if (isOwner) {
                    Intent mainIntent = new Intent(SelectionActivity.this, OwnerDashboardActivity.class);
                    startActivity(mainIntent);
                }
            }
        });


    }
}