package com.linarc.carpooling.ui.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.linarc.carpooling.ui.home.rider.RiderDashboardActivity;

public class NotificationClass {
    Context mcontext;

    public NotificationClass(Context context) {
        this.mcontext = context;
    }


   public void shownotif() {

        PendingIntent pi = PendingIntent.getActivity(mcontext, 0, new Intent(mcontext, RiderDashboardActivity.class), 0);
        Notification noti = new NotificationCompat.Builder(mcontext)
                .setContentTitle("Notification")
                .setContentText("There are new places to visit")
                .setSmallIcon(android.R.drawable.stat_sys_warning)
                .setContentIntent(pi)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .build();
        NotificationManager mgr = (NotificationManager) mcontext.getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.notify(0, noti);
    }
}
