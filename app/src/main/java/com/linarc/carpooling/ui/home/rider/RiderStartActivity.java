package com.linarc.carpooling.ui.home.rider;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.linarc.carpooling.R;

public class RiderStartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_start);
    }
}