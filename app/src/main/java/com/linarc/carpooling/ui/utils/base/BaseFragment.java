package com.linarc.carpooling.ui.utils.base;


import android.content.Context;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.linarc.carpooling.CarpoolingApplication;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.utils.NetworkUtil;

import butterknife.Unbinder;


public class BaseFragment<T> extends Fragment implements IBaseViewUtilities {

    protected View view;
    protected T mainActivity;
    protected Unbinder mUnbinder;
    private Snackbar snackbar;
    protected int mMenuIndex = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (T) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainActivity = null;

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }


    @Override
    public void setStatusBarColor(int resId) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).setStatusBarColor(resId);
        }
    }

    @Override
    public void showProgressDialog(String msg) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).showProgressDialog(msg);
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (isResumed() && !isRemoving()) {
            if (mainActivity instanceof IBaseViewUtilities) {
                ((IBaseViewUtilities) mainActivity).dismissProgressDialog();
            }
        }
    }

    @Override
    public void dismissOnResume() {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).dismissOnResume();
        }
    }

    @Override
    public void showMessage(MessageType type, MessagePayload payload, IDialogCallBack callBack) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).showMessage(type, payload, callBack);
        }
    }

    @Override
    public void runOnUIThread(IRunEventCallBack callBack) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).runOnUIThread(callBack);
        }
    }


    public void setPrimaryButton(Button primaryButton) {
        if (mainActivity instanceof BaseActivity) {
            ((BaseActivity) mainActivity).setPrimaryButton(primaryButton);
        }
    }

    public void setAsPrimaryButtonInvoker(EditText view) {
        if (mainActivity instanceof BaseActivity) {
            ((BaseActivity) mainActivity).setAsPrimaryButtonInvoker(view);
        }
    }

    public void addNextButtonCallBack(EditText view, IRunEventCallBack callBack) {
        if (mainActivity instanceof BaseActivity) {
            ((BaseActivity) mainActivity).addNextButtonCallBack(view, callBack);
        }
    }

    @Override
    public void disablePrimaryButton() {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).disablePrimaryButton();
        }
    }

    @Override
    public void enablePrimaryButton() {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).enablePrimaryButton();
        }
    }

    @Override
    public boolean InternetConnection(View view) {
        if (!NetworkUtil.isInternetavailable(CarpoolingApplication.getAppContext())) {
            showSnackBar(view);

            return false;
        }

        return true;
    }


    public void showSnackBar(View  linearLayout) {
        snackbar = Snackbar
                .make(linearLayout, "No Connection", Snackbar.LENGTH_INDEFINITE).
                        setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        }).setBackgroundTint(getResources().getColor(R.color.white))
                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        snackbar.show();
    }

    public void showHideSoftKeyBoard(Boolean show) {
        if (mainActivity instanceof BaseActivity) {
            ((BaseActivity) mainActivity).showHideSoftKeyBoard(show);
        }
    }

    public SpannableStringBuilder changeSize(String source, String toResize, int size) {
        if (!(mainActivity instanceof BaseActivity)) {
            return null;
        }

        return ((BaseActivity) mainActivity).changeSize(source, toResize, size);
    }

    public SpannableStringBuilder changeColor(String source, String toResize, int color) {
        if (!(mainActivity instanceof BaseActivity)) {
            return null;
        }

        return ((BaseActivity) mainActivity).changeColor(source, toResize, color);
    }

    public SpannableStringBuilder changeStyleToBold(String source, int start, int end) {
        if (!(mainActivity instanceof BaseActivity)) {
            return null;
        }

        return ((BaseActivity) mainActivity).changeStyleToBold(source, start, end);
    }


}
