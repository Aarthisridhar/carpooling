package com.linarc.carpooling.ui.utils;


import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.linarc.carpooling.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class CustomProgressDialog extends DialogFragment {

    private int mLayoutId;
    private String mMessage;

    private Boolean mPendingDismiss = false;

    private Boolean mIsShown = false;

    @Nullable
    @BindView(R.id.tv_msg)
    TextView tvMsg;



    private Unbinder mUnbinder;

    private ViewUtils.LoaderAnimatorV2 mLoaderAnimator;


    public CustomProgressDialog() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (mLayoutId == 0) {
            mLayoutId = R.layout.dialog_loader_v2;
        }

        View view = inflater.inflate(mLayoutId, null);
        mUnbinder = ButterKnife.bind(this, view);

        //vToRotate = (View)view.findViewById(R.id.v_to_rotate);
        tvMsg = (TextView)view.findViewById(R.id.tv_msg);

    //    mLoaderAnimator = new ViewUtils.LoaderAnimatorV2(getActivity(), vToRotate);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getContext(), R.color.transparent)));
getDialog().setCancelable(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mPendingDismiss && mIsShown) {
            super.dismiss();
            mIsShown = false;
            mPendingDismiss = false;
        }

        animate();
    }

    @Override
    public void onStart() {
        super.onStart();

        getDialog().getWindow().setLayout(ViewGroup.LayoutParams
                .WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        WindowManager.LayoutParams wlp = getDialog().getWindow().getAttributes();

        wlp.gravity = Gravity.CENTER;
        getDialog().getWindow().setAttributes(wlp);
    }

    @Override
    public void onDestroyView() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroyView();
    }

    public void setLayoutId(int layoutId) {
        mLayoutId = layoutId;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;

        if (tvMsg != null) {
            tvMsg.setText(mMessage);

            if (tvMsg.getText().toString().trim().isEmpty()) {
                tvMsg.setVisibility(View.GONE);
            } else {
                tvMsg.setVisibility(View.VISIBLE);
            }
        }

    }

    private void animate() {
        if (mLoaderAnimator != null) {
            mLoaderAnimator.animate();
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (!mIsShown) {
            super.show(manager, tag);
        }

        mIsShown = true;
    }

    public void dismissOnResume() {
        mPendingDismiss = true;
    }

    @Override
    public void dismiss() {

        if (getActivity().isFinishing()) {
            return;
        }

        if (isResumed()) {
            super.dismiss();
            mIsShown = false;
        }
    }

}
