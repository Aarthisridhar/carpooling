package com.linarc.carpooling.ui.utils.base;



public class MessagePayload {
    private String mTitle;
    private String mMessage;
    private String mNegative;
    private String mPositive;

    public static MessagePayload newInstance() {
        return new MessagePayload();
    }

    public static MessagePayload newInstance(String msg) {
        return new MessagePayload(msg);
    }

    public static MessagePayload newInstance(String msg,String positive,String negative) {
        return new MessagePayload(msg,positive,negative);
    }


    public static MessagePayload newInstance(String title,String msg,String positive,String negative) {
        return new MessagePayload(title,msg,positive,negative);
    }
    public MessagePayload(String mMessage, String mPositive, String mNegative) {
        this.mMessage = mMessage;
        this.mNegative = mNegative;
        this.mPositive = mPositive;
    }

    public MessagePayload(String mTitle, String mMessage, String mNegative, String mPositive) {
        this.mTitle = mTitle;
        this.mMessage = mMessage;
        this.mNegative = mNegative;
        this.mPositive = mPositive;
    }

    private MessagePayload() {

    }

    private MessagePayload(String msg) {
        mMessage = msg;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getNegative() {
        return mNegative;
    }

    public void setNegative(String negative) {
        mNegative = negative;
    }

    public String getPositive() {
        return mPositive;
    }

    public void setPositive(String positive) {
        mPositive = positive;
    }
}
