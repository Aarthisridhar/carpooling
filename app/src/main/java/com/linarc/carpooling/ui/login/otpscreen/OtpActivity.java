package com.linarc.carpooling.ui.login.otpscreen;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.linarc.carpooling.databinding.ActivityOtpBinding;
import com.linarc.carpooling.ui.selection.SelectionActivity;

import java.util.concurrent.TimeUnit;

public class OtpActivity extends AppCompatActivity {
    ActivityOtpBinding otpBinding;
    OtpViewModel otpViewModel;
    private String verificationId;
    boolean isverified = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        otpBinding = ActivityOtpBinding.inflate(getLayoutInflater());
        otpViewModel = new ViewModelProvider(this).get(OtpViewModel.class);
        setContentView(otpBinding.getRoot());
        otpViewModel.onInitialiseFirebase();


        otpViewModel.onverifyphonenumber.observe(OtpActivity.this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(!s.isEmpty())
                {
                    PhoneAuthProvider.verifyPhoneNumber(
                            PhoneAuthOptions
                                    .newBuilder(FirebaseAuth.getInstance())
                                    .setActivity(OtpActivity.this)
                                    .setPhoneNumber(s)
                                    .setTimeout(60L, TimeUnit.SECONDS)
                                    .setCallbacks(mCallBack)
                                    .build());
                }
            }
        });
        otpViewModel.onNextScreen.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean)
                {
                    Intent i = new Intent(OtpActivity.this, SelectionActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
        otpViewModel.onShowErrortxt.observe(OtpActivity.this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if(!s.isEmpty())
                {
                    otpBinding.txtError.setVisibility(View.VISIBLE);
                    otpBinding.txtError.setText(s);
                }else
                {
                    otpBinding.txtError.setVisibility(View.INVISIBLE);
                }
            }
        });

        otpBinding.txtResend.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                otpViewModel.onResendOtp();
            }
        });

        TextWatcher textWatcher = new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {

                otpBinding.txtError.setVisibility(View.GONE);

                int size = 1;
                String code1, code2, code3, code4, code5, code6;

                code1 = otpBinding.etCode1.getText().toString();
                code2 = otpBinding.etCode2.getText().toString();
                code3 = otpBinding.etCode3.getText().toString();
                code4 = otpBinding.etCode4.getText().toString();
                code5 = otpBinding.etCode5.getText().toString();
                code6 = otpBinding.etCode6.getText().toString();

                if (code1.length() == size) {
                    otpBinding.etCode2.requestFocus();
                }

                if (code2.length() == size) {
                    otpBinding.etCode3.requestFocus();
                }
                if (code3.length() == size) {
                    otpBinding. etCode4.requestFocus();
                }
                if (code4.length() == size) {
                    otpBinding.etCode5.requestFocus();
                }
                if (code5.length() == size) {
                    otpBinding. etCode6.requestFocus();
                }


                if (!otpBinding.etCode1.getText().toString().isEmpty()
                        && !otpBinding.etCode2.getText().toString().isEmpty()
                        && !otpBinding.etCode3.getText().toString().isEmpty()
                        && !otpBinding.etCode4.getText().toString().isEmpty()
                        && !otpBinding.etCode5.getText().toString().isEmpty()
                        && !otpBinding.etCode6.getText().toString().isEmpty()) {
                    //  showProgressDialog(Strings.EMPTY);
                   verifyCode(code1 + code2 + code3 + code4 + code5 + code6);
                }

            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        otpBinding. etCode1.addTextChangedListener(textWatcher);
        otpBinding. etCode2.addTextChangedListener(textWatcher);
        otpBinding. etCode3.addTextChangedListener(textWatcher);
        otpBinding. etCode4.addTextChangedListener(textWatcher);
        otpBinding. etCode5.addTextChangedListener(textWatcher);
        otpBinding. etCode6.addTextChangedListener(textWatcher);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
            otpViewModel.mutableverifycode.setValue(s);
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            isverified = true;
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {


                otpBinding.etCode1.setText(String.valueOf(code.charAt(0)));
                otpBinding.etCode2.setText(String.valueOf(code.charAt(1)));
                otpBinding.etCode3.setText(String.valueOf(code.charAt(2)));
                otpBinding.etCode4.setText(String.valueOf(code.charAt(3)));
                otpBinding.etCode5.setText(String.valueOf(code.charAt(4)));
                otpBinding.etCode6.setText(String.valueOf(code.charAt(5)));

                verifyCode(code);
            }
        }


        @Override
        public void onVerificationFailed(FirebaseException e) {
            // mView.dismissProgressDialog();
           otpViewModel.onShowErrortxt.setValue(e.toString());
        }
    };
    public void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        otpViewModel.signInWithCredential(credential);

    }
}