package com.linarc.carpooling.ui.home.rider;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.home.owner.OwnerDashboardActivity;
import com.linarc.carpooling.ui.home.rider.model.RidesModel;
import com.linarc.carpooling.ui.utils.PrefUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RiderAdapter extends RecyclerView.Adapter<RiderAdapter.MyViewHolder> {
    private Context context;
    private List<RidesModel> riderList;
    FirebaseFirestore db;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView carName, ownerName, carPrice, endLocation;
        public Button request;
        public ImageView profilename;

        public MyViewHolder(View view) {
            super(view);
            carName = view.findViewById(R.id.tv_car_name);
            ownerName = view.findViewById(R.id.tv_ridername);
            carPrice = view.findViewById(R.id.tv_price);
            request = view.findViewById(R.id.btn_Request);
            endLocation = view.findViewById(R.id.tv_end_location);
            profilename = view.findViewById(R.id.iv_SensorName);

        }
    }


    public RiderAdapter(Context context, List<RidesModel> riderList) {
        this.context = context;
        this.riderList = riderList;
    }

    public void reinitialze(List<RidesModel> riderList, FirebaseFirestore db) {
        this.db = db;
        this.riderList = riderList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rides, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final RidesModel ridesModel = riderList.get(position);
        holder.carName.setText(ridesModel.getCarName());
        holder.ownerName.setText(ridesModel.getFullName());
        holder.carPrice.setText(context.getResources().getString(R.string.rs)+ridesModel.getPrice());
        String endLocation = "End Destination: " + ridesModel.getEndLocation();
        holder.endLocation.setText(endLocation);

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(ridesModel.getFullName().substring(0, 1), color);
       holder. profilename.setImageDrawable(drawable);

        holder.request.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                String userFullName = PrefUtils.getUserFullName(context);
                Map<String, Object> rideRequest = new HashMap<>();
                rideRequest.put("status", "REQ");
                rideRequest.put("fullName", userFullName);

                db.collection("rides")
                    .document(ridesModel.getTripId())
                    .collection("riders")
                    .add(rideRequest)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference)
                        {

                            String riderId = documentReference.getId();
                            PrefUtils.saveRiderId(context,riderId);
                            PrefUtils.saveRideId(context, ridesModel.getTripId());

                            PrefUtils.saveUserRole(context,"rider");
                            Intent i = new Intent(context,RiderStatusActivity.class);
                            context.startActivity(i);
                            //Log.d("Success", "DocumentSnapshot written with ID: " + documentReference.getId());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("Failed", "Error adding document", e);
                        }
                    });
            }
        });
    }

    @Override
    public int getItemCount() {
        return riderList.size();
    }


}

   
