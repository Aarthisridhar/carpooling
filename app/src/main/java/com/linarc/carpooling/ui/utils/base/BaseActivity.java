package com.linarc.carpooling.ui.utils.base;


import static com.google.common.base.Strings.isNullOrEmpty;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.util.Preconditions;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.linarc.carpooling.CarpoolingApplication;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.utils.CustomProgressDialog;
import com.linarc.carpooling.ui.utils.LogUtils;
import com.linarc.carpooling.ui.utils.NetworkUtil;
import com.linarc.carpooling.ui.utils.ViewUtils;


import java.lang.ref.WeakReference;

import butterknife.BindString;


public class BaseActivity extends AppCompatActivity implements IBaseViewUtilities {

    private WeakReference<BaseActivity> mBaseActivityWeakReference;

    private CustomProgressDialog progressDialog;
    private Dialog dialog;


    private Boolean isSaveInstanceStateCalled = false;
    public boolean isInternetConnected = false;

    private Button mPrimaryButton;

    private int mStatusBarColor;
    private Snackbar snackbar;


    private ActivityResultCallBack mResultCallBack;

    @BindString(R.string.default_loading_msg)
    String strDefaultLoadingMsg;

    private ViewUtils.LoaderAnimatorV2 mLoaderAnimator;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mBaseActivityWeakReference = new WeakReference<>(this);



        setUpProgressDialog();


    }


    private void setUpProgressDialog() {
        progressDialog = ViewUtils.getNewInstanceProgressDialog(mBaseActivityWeakReference.get(), null);
    }

    @Override
    public void setStatusBarColor(int resId) {
        mStatusBarColor = resId;
        onStatusBarColorChanged();
    }

    private void onStatusBarColorChanged() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(ContextCompat.getColor(this, mStatusBarColor));
        }
    }


    /**
     * show progress dialog with custom msg if not null || empty
     */
    @Override
    public void showProgressDialog(String msg) {

        if (progressDialog != null) {

            if (!isNullOrEmpty(msg)) {
                progressDialog.setMessage(msg);
            } else {
                progressDialog.setMessage("");
            }

            if (!isFinishing()) {
                BaseActivity activity = mBaseActivityWeakReference.get();
                progressDialog.show(activity.getSupportFragmentManager(), progressDialog.getClass().getSimpleName());
            }
        }
    }

    /**
     * dismiss progress dialog
     */
    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.getActivity() != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void dismissOnResume() {
        if (progressDialog != null && progressDialog.getActivity() != null) {
            progressDialog.dismissOnResume();
        }
    }

    /**
     * show message as {type} with string
     */
    @SuppressLint("RestrictedApi")
    @Override
    public void showMessage(MessageType type, MessagePayload payload, IDialogCallBack callBack) {
        Preconditions.checkNotNull(payload);

        switch (type) {
            case TOAST:
                if (!isNullOrEmpty(payload.getMessage())) {
                    Toast.makeText(this, payload.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    LogUtils.i("TOAST", "Message is empty");
                }
                break;
            case DIALOG:
                dialog = ViewUtils.buildGenericDialog(this, payload.getTitle(), payload.getMessage(), payload.getPositive(),
                        payload.getNegative(), callBack);
                dialog.show();
                break;

        }
    }

    @Override
    public void runOnUIThread(IRunEventCallBack callBack) {
        ViewUtils.runOnUIThread(callBack);
    }

    /**
     * getters
     */
    public CustomProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public Dialog getDialog() {
        return dialog;
    }





    public Button getPrimaryButton() {
        return mPrimaryButton;
    }

    public void setPrimaryButton(Button primaryButton) {
        mPrimaryButton = primaryButton;
    }

    @Override
    public void disablePrimaryButton() {
        if (mPrimaryButton != null) {
            mPrimaryButton.post(new Runnable() {
                @Override
                public void run() {
                    mPrimaryButton.setEnabled(false);
                }
            });
        }
    }

    @Override
    public void enablePrimaryButton() {
        if (mPrimaryButton != null) {
            mPrimaryButton.setEnabled(true);
        }
    }

    @Override
    public boolean InternetConnection(View view) {
        if (!NetworkUtil.isInternetavailable(CarpoolingApplication.getAppContext())) {
            showSnackBar(view);
            isInternetConnected = false;
            return false;
        }
        isInternetConnected = true;
        return true;
    }

    public void setAsPrimaryButtonInvoker(EditText view) {
        view.setOnEditorActionListener(getOnTextEditorActionListenerForPrimaryButton());
    }

    public void addNextButtonCallBack(EditText view, IRunEventCallBack callBack) {
        view.setOnEditorActionListener(getOnTextEditorActionListenerForNext(callBack));
    }

    /**
     * show hide kb
     */
    public void showHideSoftKeyBoard(Boolean show) {
        ViewUtils.showHideSoftKeyBoard(this, this.getCurrentFocus(), show);
    }

    /**
     * show hide kb for dialog fragment
     */
    public void showHideSoftKeyBoard(Dialog dialog, Boolean show) {
        ViewUtils.showHideSoftKeyBoard(this, dialog.getCurrentFocus(), show);
    }

    private TextView.OnEditorActionListener getOnTextEditorActionListenerForPrimaryButton() {
        return new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_GO) {
                    mPrimaryButton.performClick();
                }

                return false;
            }
        };
    }

    private TextView.OnEditorActionListener getOnTextEditorActionListenerForNext(final IRunEventCallBack callBack) {
        return new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (callBack != null) {
                        callBack.start();
                    }
                }

                return false;
            }
        };
    }


    public void setUpLoaderV2(final View toRotate) {
        mLoaderAnimator = new ViewUtils.LoaderAnimatorV2(this, toRotate);
    }

    public void animateLoader() {
        if (mLoaderAnimator != null) {
            mLoaderAnimator.animate();
        }
    }

    protected ValueAnimator newColorAnimator(final TextView v, int toColor) {

        final int color = ContextCompat.getColor(this, toColor);
        final ValueAnimator colorAnim = ObjectAnimator.ofFloat(0f, 1f);
        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float mul = (Float) animation.getAnimatedValue();
                int alphaColor = adjustAlpha(color, mul);

                Drawable drawable = v.getBackground();
                drawable.setColorFilter(alphaColor, PorterDuff.Mode.SRC_ATOP);

                if (mul == 0.0) {
                    drawable.setColorFilter(null);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    v.setBackground(drawable);
                } else {
                    v.setCompoundDrawables(null, null, drawable, null);
                }

            }
        });

        colorAnim.setDuration(1000);
        colorAnim.setRepeatMode(ValueAnimator.REVERSE);
        colorAnim.setRepeatCount(-1);

        return colorAnim;
    }

    protected void removeTextViewBackgroundFilter(TextView v) {
        Drawable drawable = v.getBackground();
        drawable.setColorFilter(null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackground(drawable);
        } else {
            v.setCompoundDrawables(null, null, drawable, null);
        }
    }

    private int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    protected SpannableStringBuilder changeSize(String source, String toResize, int size) {

        SpannableStringBuilder sb = null;

        try {
            if (!(source.isEmpty() && toResize.isEmpty())) {
                int preTextIndex = source.indexOf(toResize);
                sb = new SpannableStringBuilder(source);
                sb.setSpan(new AbsoluteSizeSpan(size), preTextIndex, preTextIndex + toResize.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
        } catch (IndexOutOfBoundsException ex) {
            LogUtils.e(BaseActivity.class.getSimpleName(), "text changesize error");
        }

        return sb;
    }

    protected SpannableStringBuilder changeColor(String source, String toChange, int color) {

        SpannableStringBuilder sb = null;

        try {
            if (!(source.isEmpty() && toChange.isEmpty())) {
                int preTextIndex = source.indexOf(toChange);
                sb = new SpannableStringBuilder(source);
                sb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, color)), preTextIndex, preTextIndex + toChange.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
        } catch (IndexOutOfBoundsException ex) {
            LogUtils.e(BaseActivity.class.getSimpleName(), "text changecolor error");
        }

        return sb;
    }

    protected SpannableStringBuilder changeStyleToBold(String source, int start, int end) {

        SpannableStringBuilder sb = null;

        try {
            if (!(source.isEmpty())) {
                sb = new SpannableStringBuilder(source);
                StyleSpan bold = new StyleSpan(android.graphics.Typeface.BOLD);
                sb.setSpan(bold, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            }
        } catch (IndexOutOfBoundsException ex) {
            LogUtils.e(BaseActivity.class.getSimpleName(), "text change style error");
        }

        return sb;
    }


    public void showSnackBar(View view) {
        snackbar = Snackbar
                .make(view, "No Connection", Snackbar.LENGTH_INDEFINITE).
                        setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        }).setBackgroundTint(getResources().getColor(R.color.white))
                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        snackbar.show();
    }


    public void setResultCallBack(ActivityResultCallBack resultCallBack) {
        mResultCallBack = resultCallBack;
    }

    public interface ActivityResultCallBack {
        void result(int requestCode, int resultCode, Intent data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mResultCallBack != null) {
            mResultCallBack.result(requestCode, resultCode, data);
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        isSaveInstanceStateCalled = true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        isSaveInstanceStateCalled = false;

    }

    public Boolean isSaveInstanceStateCalled() {
        return isSaveInstanceStateCalled;
    }

    public String getUid()
    {
        return FirebaseAuth.getInstance().getUid();
    }
}
