package com.linarc.carpooling.ui.login.otpscreen;

public class UserModel {
    private String uid;
    private String mobilenumber;
    private String timestamp;
    private boolean isRider = false;

    public UserModel(String uid, String mobilenumber, String timestamp, boolean isRider) {
        this.uid = uid;
        this.mobilenumber = mobilenumber;
        this.timestamp = timestamp;
        this.isRider = isRider;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getMobilenumber() {
        return mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        this.mobilenumber = mobilenumber;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isRider() {
        return isRider;
    }

    public void setRider(boolean rider) {
        isRider = rider;
    }
}
