package com.linarc.carpooling.ui.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.core.content.ContextCompat;

import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.utils.base.IDialogCallBack;
import com.linarc.carpooling.ui.utils.base.IRunEventCallBack;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ViewUtils {
    static int position;
    static AlertDialog dialog;

    public static CustomProgressDialog getNewInstanceProgressDialog(final Context context, String strMsg) {
        String msg = context.getString(R.string.default_loading_msg);

        if (strMsg != null && !strMsg.isEmpty()) {
            msg = strMsg;
        }

        CustomProgressDialog progressBar = new CustomProgressDialog();
        progressBar.setCancelable(false);
        progressBar.setMessage(msg);

        return progressBar;
    }

    public static class LoaderAnimatorV2 {

        private Context mContext;
        private View mToRotate;


        private RotateAnimation mRotate;

        public LoaderAnimatorV2(Context context, View toRotate) {
            mContext = context;
            mToRotate = toRotate;

            mRotate = new RotateAnimation(0, 360,
                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                    0.5f);

            mRotate.setDuration(1000);
            mRotate.setRepeatCount(Animation.INFINITE);
        }

        public void animate() {
            mToRotate.startAnimation(mRotate);
        }

        public void stop() {
            if (mToRotate != null) {
                mToRotate.clearAnimation();
            }
        }
    }

    public static void showHideSoftKeyBoard(Context context, View view, Boolean show) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (inputMethodManager != null && view != null) {
            if (show) {
                inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
            } else {
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }

    }


    public static void runOnUIThread(final IRunEventCallBack callBack) {


        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                callBack.start();
            }
        });
    }

    public static Dialog buildGenericDialog(Context context, String title, String msg, String positiveStringLabel, String negativeStringLabel,
                                            final IDialogCallBack callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);


        LayoutInflater inflater = LayoutInflater.from(context);

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.85);
        int height = (int) (context.getResources().getDisplayMetrics().heightPixels * 0.33);
        final Dialog d = new Dialog(context, R.style.Theme_CarPooling);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = d.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.dimAmount = 0.70f;
        wlp.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        wlp.width = width;
        wlp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        window.setAttributes(wlp);
        d.setCanceledOnTouchOutside(false);

        View view = inflater.inflate(R.layout.dialog_generic_with_cancel_layout, null);
        TextView header = (TextView) view.findViewById(R.id.custom_dialog_header);
        TextView description = (TextView) view.findViewById(R.id.custom_dialog_content);
        ImageView imageView = (ImageView) view.findViewById(R.id.custom_dialog_check);
        imageView.setVisibility(View.GONE);
        Button okButton = (Button) view.findViewById(R.id.custom_ok_btn);
        Button cancelButton = (Button) view.findViewById(R.id.custom_cancel_btn);
        d.setContentView(view);

        header.setVisibility(View.GONE);
        if (title != null && !title.isEmpty()) {
            header.setText(title);
            header.setVisibility(View.VISIBLE);
        }

        description.setText(msg);
        okButton.setText(positiveStringLabel);
        cancelButton.setText(negativeStringLabel);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.positiveButtonClick();
                }
                d.dismiss();
            }
        });


        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.negativeButtonClick();
                }

                d.dismiss();
            }
        });

        if (TextUtils.isEmpty(negativeStringLabel)) {
            cancelButton.setVisibility(View.GONE);
            okButton.setBackground(context.getResources().getDrawable(R.drawable.selector_otp_primary_button));
        }

        return d;

    }

    public static void showDateTimePickerDialog(final Context context, final OnTimePickerListener onTimePickerListener) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.dialog_datetimepicker, null);


        final DatePicker _datePicker = view.findViewById(R.id.dtp__date);
        final TimePicker _timePicker = view.findViewById(R.id.tp__time);
        _datePicker.setMinDate(new DateTime().minusDays(13).getMillis());
        _datePicker.setMaxDate(new DateTime().getMillis());
      /*  _timePicker.setHour(hours);
        _timePicker.setMinute(minutes);*/
        _timePicker.setIs24HourView(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        builder.setNegativeButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onTimePickerListener.OnNegativeButtonClick();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();

        Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        okButton.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        Button cancelButton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        cancelButton.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        //Time validation
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  DateTime today = new DateTime(minTime.withTimeAtStartOfDay());
                int day = _datePicker.getDayOfMonth();
                int month = _datePicker.getMonth();
                int year = _datePicker.getYear();


                Calendar cal = Calendar.getInstance();
                cal.set(year, month, day);
                // cal.add(Calendar.DAY_OF_MONTH, 13);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                String strDate = format.format(cal.getTime());

                String timeSet = "";
                int hour = _timePicker.getHour();
                if (hour > 12) {
                    hour -= 12;
                    timeSet = "PM";
                } else if (hour == 0) {
                    hour += 12;
                    timeSet = "AM";
                } else if (hour == 12)
                    timeSet = "PM";
                else
                    timeSet = "AM";
                String selectedTime = String.format("%02d:%02d", hour, _timePicker.getMinute());

                DateTime selectedTimeStamp = new DateTime(cal.getTimeInMillis()).withTimeAtStartOfDay().plusHours(_timePicker.getHour()).plusMinutes(_timePicker.getMinute());

                onTimePickerListener.OnPositiveBtnClick(strDate, selectedTime, timeSet, selectedTimeStamp);
                dialog.dismiss();
            }
        });
    }





    public interface OnTimePickerListener {
        void OnPositiveButtonClick(DateTime time);

        void OnNegativeButtonClick();

        void OnPositiveBtnClick(String date, String strDate, String selectedTime, DateTime selectedDateTime);
    }



    public static boolean checkInternetConnection(Context context) {
        ConnectivityManager ConnectionManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (ConnectionManager != null) {
            networkInfo = ConnectionManager.getActiveNetworkInfo();
        }
        return networkInfo != null && networkInfo.isConnected();
    }



    public static void showTimePickerDialog(final Context context, String title, int hours, int minutes, final DateTime minTime, final DateTime maxTime, final OnTimePickerListener onTimePickerListener) {
        int currentNightMode = context.getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.dialog_timepicker, null);

        TextView _timePickerTitle = view.findViewById(R.id.txt_time_title);
        _timePickerTitle.setText(title);

        final TimePicker _timePicker = view.findViewById(R.id.tp_time_picker);
        _timePicker.setHour(hours);
        _timePicker.setMinute(minutes);
        _timePicker.setIs24HourView(false);

        final AlertDialog.Builder builder;
        if (currentNightMode == Configuration.UI_MODE_NIGHT_NO) {
            builder = new AlertDialog.Builder(context, R.style.AlertListDialogLight);
        } else {
            builder = new AlertDialog.Builder(context, R.style.AlertListDialogDark);
        }
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onTimePickerListener.OnNegativeButtonClick();
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
        Button okButton = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
        okButton.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        Button cancelButton = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        cancelButton.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

        //Time validation
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DateTime today = new DateTime(minTime.withTimeAtStartOfDay());
                DateTime selectedTime = today.plusHours(_timePicker.getHour()).plusMinutes(_timePicker.getMinute());
                DateTimeFormatter fmt = DateTimeFormat.forPattern("hh:mm a");

                if (selectedTime.compareTo(maxTime) > 0)
                {
                    _timePicker.setHour(hours);
                    _timePicker.setMinute(minutes);
                    return;
                }

                if (selectedTime.compareTo(minTime) <= 0) {
                    _timePicker.setHour(hours);
                    _timePicker.setMinute(minutes);
                    return;
                }

                onTimePickerListener.OnPositiveButtonClick(selectedTime);
                dialog.dismiss();
            }
        });
    }


}
