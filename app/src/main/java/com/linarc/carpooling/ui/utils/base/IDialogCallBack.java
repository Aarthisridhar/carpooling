package com.linarc.carpooling.ui.utils.base;



public interface IDialogCallBack {
    void positiveButtonClick();

    void negativeButtonClick();
}
