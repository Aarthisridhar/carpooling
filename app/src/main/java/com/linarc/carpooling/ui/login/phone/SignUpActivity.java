package com.linarc.carpooling.ui.login.phone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.auth.FirebaseUser;
import com.linarc.carpooling.databinding.ActivityPhoneBinding;
import com.linarc.carpooling.ui.selection.SelectionActivity;
import com.linarc.carpooling.ui.utils.base.BaseActivity;

public class SignUpActivity extends BaseActivity {


    SignUpViewModel phoneViewModel;
    ActivityPhoneBinding phoneBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        phoneBinding = ActivityPhoneBinding.inflate(getLayoutInflater());
        phoneViewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
        setContentView(phoneBinding.getRoot());
        phoneBinding.setViewmodel(phoneViewModel);
        phoneViewModel.showError.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.isEmpty() && s.contains("Mail")) {
                    phoneBinding.tvMailId.setError(s);
                }

                if (!s.isEmpty() && s.contains("Name")) {
                    phoneBinding.tvFullName.setError(s);
                }
                if (!s.isEmpty() && s.contains("Password")) {
                    phoneBinding.password.setError(s);
                }
                if (!s.isEmpty() && s.contains("mobile")) {
                    phoneBinding.etPhoneNumber.setError(s);
                }
            }
        });


        phoneViewModel.progressdialog.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean)
                {
                    showProgressDialog("");
                }else
                {
                    dismissProgressDialog();
                }
            }
        });
        phoneViewModel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {

                if (firebaseUser != null) {

                    Toast.makeText(SignUpActivity.this, "Registration Successfully", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(SignUpActivity.this, SelectionActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });


        phoneBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}
