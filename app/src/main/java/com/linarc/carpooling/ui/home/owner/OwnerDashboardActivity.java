package com.linarc.carpooling.ui.home.owner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.home.ridelist.RiderListActivity;
import com.linarc.carpooling.ui.home.rider.RiderDashboardActivity;
import com.linarc.carpooling.ui.selection.SelectionActivity;
import com.linarc.carpooling.ui.utils.PrefUtils;
import com.linarc.carpooling.ui.utils.base.BaseActivity;

import java.util.HashMap;
import java.util.Map;

public class OwnerDashboardActivity extends BaseActivity {
    TextView seat1, seat2, seat3, seat4, seat5, seat6, seat7;
    Button startRide;
    ImageButton back;
    TextInputLayout startLocation, endLocation, carName, price;
    int seatCount = 0;
    FirebaseFirestore db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_dashboard);

        db = FirebaseFirestore.getInstance();
        seat1 = (TextView) findViewById(R.id.ib_seat1);
        seat2 = (TextView) findViewById(R.id.ib_seat2);
        seat3 = (TextView) findViewById(R.id.ib_seat3);
        seat4 = (TextView) findViewById(R.id.ib_seat4);
        seat5 = (TextView) findViewById(R.id.ib_seat5);
        seat6 = (TextView) findViewById(R.id.ib_seat6);
        seat7 = (TextView) findViewById(R.id.ib_seat7);
        startRide = (Button) findViewById(R.id.lyt_send);
        back = (ImageButton) findViewById(R.id.back);
        startLocation = (TextInputLayout) findViewById(R.id.tv_start_location);
        endLocation = (TextInputLayout) findViewById(R.id.tv_end_location);
        carName = (TextInputLayout) findViewById(R.id.tv_car_name);
        price = (TextInputLayout) findViewById(R.id.tv_price);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        seat1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                resetSeatColor();
                seat1.setBackgroundResource(R.drawable.rectangle_blue_border);
                seat1.setTextColor(Color.parseColor("#FFFFFF"));
                seatCount = 1;
            }
        });

        seat2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                resetSeatColor();
                seat2.setBackgroundResource(R.drawable.rectangle_blue_border);
                seat2.setTextColor(Color.parseColor("#FFFFFF"));
                seatCount = 2;
            }
        });

        seat3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                resetSeatColor();
                seat3.setBackgroundResource(R.drawable.rectangle_blue_border);
                seat3.setTextColor(Color.parseColor("#FFFFFF"));
                seatCount = 3;
            }
        });

        seat4.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                resetSeatColor();
                seat4.setBackgroundResource(R.drawable.rectangle_blue_border);
                seat4.setTextColor(Color.parseColor("#FFFFFF"));
                seatCount = 4;
            }
        });

        seat5.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                resetSeatColor();
                seat5.setBackgroundResource(R.drawable.rectangle_blue_border);
                seat5.setTextColor(Color.parseColor("#FFFFFF"));
                seatCount = 5;
            }
        });

        seat6.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                resetSeatColor();
                seat6.setBackgroundResource(R.drawable.rectangle_blue_border);
                seat6.setTextColor(Color.parseColor("#FFFFFF"));
                seatCount = 6;
            }
        });

        seat7.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                resetSeatColor();
                seat7.setBackgroundResource(R.drawable.rectangle_blue_border);
                seat7.setTextColor(Color.parseColor("#FFFFFF"));
                seatCount = 7;
            }
        });

        startRide.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                String startLocationText = startLocation.getEditText().getText().toString();
                String endLocationText = endLocation.getEditText().getText().toString();
                String carNameText = carName.getEditText().getText().toString();
                String priceText = price.getEditText().getText().toString();

                if(startLocationText.isEmpty() || endLocationText.isEmpty()||carNameText.isEmpty()||priceText.isEmpty())
                {
                    Toast.makeText(OwnerDashboardActivity.this, "Please Enter All the Field", Toast.LENGTH_SHORT).show();
                }else
                {
                    addTripDataToFirestore(carNameText, priceText, startLocationText, endLocationText);
                }


            }
        });
    }

    private void resetSeatColor(){
        seat1.setBackgroundResource(R.drawable.rectangle_border);
        seat1.setTextColor(Color.parseColor("#000000"));

        seat2.setBackgroundResource(R.drawable.rectangle_border);
        seat2.setTextColor(Color.parseColor("#000000"));

        seat3.setBackgroundResource(R.drawable.rectangle_border);
        seat3.setTextColor(Color.parseColor("#000000"));

        seat4.setBackgroundResource(R.drawable.rectangle_border);
        seat4.setTextColor(Color.parseColor("#000000"));

        seat5.setBackgroundResource(R.drawable.rectangle_border);
        seat5.setTextColor(Color.parseColor("#000000"));

        seat6.setBackgroundResource(R.drawable.rectangle_border);
        seat6.setTextColor(Color.parseColor("#000000"));

        seat7.setBackgroundResource(R.drawable.rectangle_border);
        seat7.setTextColor(Color.parseColor("#000000"));
    }

    private void addTripDataToFirestore(String carName, String price, String startLocation, String endLocation){
        showProgressDialog("");
        String name = PrefUtils.getUserFullName(this);
        Map<String, Object> data = new HashMap<>();

        data.put("carName", carName);
        data.put("price", price);
        data.put("startLocation", startLocation);
        data.put("endLocation", endLocation);
        data.put("capacity", seatCount);
        data.put("status", "ST");
        data.put("userFullName", name);

        db.collection("rides")
                .add(data)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        dismissProgressDialog();
                        PrefUtils.saveRideId(OwnerDashboardActivity.this, documentReference.getId());
                        PrefUtils.saveUserRole(OwnerDashboardActivity.this,"owner");

                        Intent mainIntent = new Intent(OwnerDashboardActivity.this, RiderListActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dismissProgressDialog();
                        Log.w("ERROR", "Error adding document", e);
                    }
                });
    }
}