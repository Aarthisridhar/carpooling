package com.linarc.carpooling.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.home.ridelist.RiderListActivity;
import com.linarc.carpooling.ui.home.rider.RiderStatusActivity;
import com.linarc.carpooling.ui.login.LoginActivity;
import com.linarc.carpooling.ui.selection.SelectionActivity;
import com.linarc.carpooling.ui.utils.PrefUtils;


public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashnew);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (FirebaseAuth.getInstance().getUid() != null)
                {
                    String rideId = PrefUtils.getRideId(SplashActivity.this);
                    String userRole = PrefUtils.getUserRole(SplashActivity.this);
                    Intent mainIntent = null;


                    if(userRole.isEmpty()){
                        mainIntent = new Intent(SplashActivity.this, SelectionActivity.class);
                    }
                    else if(userRole.equalsIgnoreCase("owner")) {
                        mainIntent = new Intent(SplashActivity.this, RiderListActivity.class);
                    }
                    else if(userRole.equalsIgnoreCase("rider"))
                    {
                        mainIntent = new Intent(SplashActivity.this, RiderStatusActivity.class);

                    }
                    startActivity(mainIntent);
                    finish();
                }
                else {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

}
