package com.linarc.carpooling.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.firebase.auth.FirebaseUser;
import com.linarc.carpooling.databinding.ActivityLoginBinding;
import com.linarc.carpooling.ui.login.phone.SignUpActivity;
import com.linarc.carpooling.ui.login.viewmodel.LoggedInViewModel;
import com.linarc.carpooling.ui.selection.SelectionActivity;
import com.linarc.carpooling.ui.utils.base.BaseActivity;

public class LoginActivity extends BaseActivity {
    ActivityLoginBinding activityLoginBinding;
    LoggedInViewModel loggedInViewModel;
    private MutableLiveData<Boolean> progressdialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = ActivityLoginBinding.inflate(getLayoutInflater());
        loggedInViewModel = new ViewModelProvider(this).get(LoggedInViewModel.class);
        setContentView(activityLoginBinding.getRoot());
        activityLoginBinding.setViewmodel(loggedInViewModel);

        loggedInViewModel.showError.observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.isEmpty() && s.contains("Mail")) {
                    activityLoginBinding.tvMailId.setError(s);
                }
                if (!s.isEmpty() && s.contains("Password")) {
                    activityLoginBinding.password.setError(s);
                }

            }
        });

        loggedInViewModel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser.getUid()!=null && !firebaseUser.getUid().isEmpty()) {
                    Intent mainIntent = new Intent(LoginActivity.this, SelectionActivity.class);
                    startActivity(mainIntent);
                    finish();

                }
            }
        });

        activityLoginBinding.register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(mainIntent);

            }
        });

     loggedInViewModel.getProgressdialog().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(aBoolean)
                {
                    showProgressDialog("");
                }else
                {
                    dismissProgressDialog();
                }
            }
        });
    }
}