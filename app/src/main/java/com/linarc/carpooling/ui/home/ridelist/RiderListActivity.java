package com.linarc.carpooling.ui.home.ridelist;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.linarc.carpooling.R;
import com.linarc.carpooling.databinding.ActivityRiderListBinding;
import com.linarc.carpooling.ui.home.owner.OwnerDashboardActivity;
import com.linarc.carpooling.ui.home.ridelist.fragment.RidesRequestFragment;
import com.linarc.carpooling.ui.home.ridelist.fragment.YoursRidesFragment;
import com.linarc.carpooling.ui.home.rider.model.RidesModel;
import com.linarc.carpooling.ui.selection.SelectionActivity;
import com.linarc.carpooling.ui.utils.PrefUtils;

import java.util.ArrayList;

public class RiderListActivity extends AppCompatActivity {
    Fragment fragment;
    RidesRequestFragment ridesRequestFragment;
    YoursRidesFragment yoursRidesFragment;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    ActivityRiderListBinding activityRiderBinding;
    RiderListViewmodel riderListViewmodel;
    FirebaseFirestore db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityRiderBinding = ActivityRiderListBinding.inflate(getLayoutInflater());
        riderListViewmodel = new ViewModelProvider(this).get(RiderListViewmodel.class);
        setContentView(activityRiderBinding.getRoot());

        ridesRequestFragment = new RidesRequestFragment();
        yoursRidesFragment = new YoursRidesFragment();
        db = FirebaseFirestore.getInstance();


        activityRiderBinding.viewRequest.setBackgroundColor(getResources().getColor(R.color.transparent));
        activityRiderBinding.viewYoursrides.setBackgroundColor(getResources().getColor(R.color.btn_colour));
        activityRiderBinding.tvRequest.setTextColor(getResources().getColor(R.color.grey));
        activityRiderBinding.tvYoursrides.setTextColor(getResources().getColor(R.color.black_c));
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        activityRiderBinding.container.setAdapter(mSectionsPagerAdapter);
        riderListViewmodel.onGetRidesDetails();

        riderListViewmodel.getMutablerideslist().observe(this, new Observer<ArrayList<RidesModel>>() {
            @Override
            public void onChanged(ArrayList<RidesModel> ridesModels) {
                if (ridesModels.size() > 0) {
                    activityRiderBinding.tvFromTo.setText(ridesModels.get(0).getCarName());
                    activityRiderBinding.tvPrice.setText(String.valueOf(ridesModels.get(0).getPrice()));
                }
            }
        });
        activityRiderBinding.llYoursRides.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityRiderBinding.container.setCurrentItem(0);

            }
        });
        activityRiderBinding.llRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityRiderBinding.container.setCurrentItem(1);


            }
        });

        activityRiderBinding.container.setCurrentItem(0);
        activityRiderBinding.container.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0: {
                        activityRiderBinding.tvRequest.setTextColor(getResources().getColor(R.color.grey));
                        activityRiderBinding.tvYoursrides.setTextColor(getResources().getColor(R.color.black_c));
                        activityRiderBinding.viewRequest.setBackgroundColor(getResources().getColor(R.color.transparent));
                        activityRiderBinding.viewYoursrides.setBackgroundColor(getResources().getColor(R.color.btn_colour));
                    }
                    break;
                    case 1: {

                        activityRiderBinding.tvRequest.setTextColor(getResources().getColor(R.color.black_c));
                        activityRiderBinding.tvYoursrides.setTextColor(getResources().getColor(R.color.grey));
                        activityRiderBinding.viewYoursrides.setBackgroundColor(getResources().getColor(R.color.transparent));
                        activityRiderBinding.viewRequest.setBackgroundColor(getResources().getColor(R.color.btn_colour));
                    }
                    break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        activityRiderBinding.btnEndRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tripId = PrefUtils.getRideId(RiderListActivity.this);

                DocumentReference ownerRef = db.collection("rides").document(tripId);
                ownerRef
                    .update("status", "END")
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            PrefUtils.saveUserRole(RiderListActivity.this,"");
                            PrefUtils.saveRideId(RiderListActivity.this, "");

                            Intent mainIntent = new Intent(RiderListActivity.this, SelectionActivity.class);
                            startActivity(mainIntent);
                            finish();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("Failed", "Error updating document", e);
                        }
                    });
            }
        });

        fetchTripInfo();
    }

    private void fetchTripInfo(){
        String tripId = PrefUtils.getRideId(this);

        DocumentReference docRef = db.collection("rides").document(tripId);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        String startLocation = document.getData().get("startLocation").toString();
                        String endLocation = document.getData().get("endLocation").toString();
                        String price = document.getData().get("price").toString();

                        String completeLocation = startLocation + " - " + endLocation;
                        String priceText = RiderListActivity.this.getResources().getString(R.string.rs) + price;

                        activityRiderBinding.tvFromToLocation.setText(completeLocation);
                        activityRiderBinding.tvPrice.setText(priceText);
                    } else {
                        Log.d("Empty", "No such document");
                    }
                } else {
                    Log.d("Failed", "get failed with ", task.getException());
                }
            }
        });
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {


            switch (position) {
                case 0:
                    fragment = yoursRidesFragment;

                    break;

                case 1:
                    fragment = ridesRequestFragment;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}