package com.linarc.carpooling.ui.home.rider.model;

public class RidesModel {
    private int capacity;
    private String carName;
    private String createdDate;
    private String price;
    private String rideId;
    private String rideType;
    private String status;
    private String userId;
    private String fullName;
    private String endLocation;
    private String tripId;

    public RidesModel() {
    }

    public RidesModel(int capacity, String carName, String createdDate, String price, String rideId, String rideType, String status, String userId, String fullName, String endLocation, String tripId) {
        this.capacity = capacity;
        this.carName = carName;
        this.createdDate = createdDate;
        this.price = price;
        this.rideId = rideId;
        this.rideType = rideType;
        this.status = status;
        this.userId = userId;
        this.fullName = fullName;
        this.endLocation = endLocation;
        this.tripId = tripId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getRideType() {
        return rideType;
    }

    public void setRideType(String rideType) {
        this.rideType = rideType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }
}
