package com.linarc.carpooling.ui.login.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseUser;
import com.linarc.carpooling.ui.login.AuthAppRepository;

public class LoggedInViewModel extends AndroidViewModel {
    private AuthAppRepository authAppRepository;
    private MutableLiveData<FirebaseUser> userLiveData;
    private MutableLiveData<Boolean> loggedOutLiveData;
    private MutableLiveData<Boolean> progressdialog;
    public ObservableField<String> etpassword;
    public ObservableField<String> etmailid;
    public MutableLiveData<String> showError;


    public LoggedInViewModel(@NonNull Application application) {
        super(application);
        etpassword = new ObservableField<>("");
        etmailid = new ObservableField<>("");
        authAppRepository = new AuthAppRepository(application);
        userLiveData = authAppRepository.getUserLiveData();
        loggedOutLiveData = authAppRepository.getLoggedOutLiveData();
        showError = new MutableLiveData<>();
        progressdialog = authAppRepository.getProgressdialog();

    }

    public void logOut() {
        authAppRepository.logOut();
    }

    public void login(String email, String password) {
        authAppRepository.login(email, password);
    }

    public MutableLiveData<FirebaseUser> getUserLiveData() {
        return userLiveData;
    }

    public MutableLiveData<Boolean>getProgressdialog()
    {
        return progressdialog;
    }

    public MutableLiveData<Boolean> getLoggedOutLiveData() {
        return loggedOutLiveData;
    }

    public void onSignIn() {
        if (etmailid.get().isEmpty()) {
            showError.postValue("Please Enter vaild Mail id");
            return;
        }
        if (etpassword.get().isEmpty()) {
            showError.postValue("Please Enter Password");
            return;
        }

        login(etmailid.get().trim(),etpassword.get());

    }
}
