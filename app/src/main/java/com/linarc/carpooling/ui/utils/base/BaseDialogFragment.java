package com.linarc.carpooling.ui.utils.base;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.snackbar.Snackbar;
import com.linarc.carpooling.CarpoolingApplication;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.utils.NetworkUtil;

import butterknife.Unbinder;


public class BaseDialogFragment<T> extends DialogFragment implements IBaseViewUtilities{

    protected T mainActivity;
    protected Unbinder mUnbinder;
    private Snackbar snackbar;
    protected View view;

    protected Boolean mFullScreen = false;

    protected int mMenuIndex = -1;

    @SuppressWarnings("unchecked")
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (T) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mainActivity = null;

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog ret = super.onCreateDialog(savedInstanceState);

        if (mFullScreen) {
            int width = (getResources().getDisplayMetrics().widthPixels);
            int height = (getResources().getDisplayMetrics().heightPixels);

            ret = new Dialog(getContext(), R.style.Theme_CarPooling);
            ret.requestWindowFeature(Window.FEATURE_NO_TITLE);

            Window window = ret.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();

            wlp.width = width;
            wlp.height = height;

            window.setAttributes(wlp);
        }

        return ret;
    }


    @Override
    public void setStatusBarColor(int resId) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).setStatusBarColor(resId);
        }
    }

    @Override
    public void showProgressDialog(String msg) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).showProgressDialog(msg);
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (mainActivity instanceof IBaseViewUtilities) {
            if (isResumed()) {
                ((IBaseViewUtilities) mainActivity).dismissProgressDialog();
            }
        }
    }

    @Override
    public void dismissOnResume() {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).dismissOnResume();
        }
    }

    @Override
    public void showMessage(MessageType type, MessagePayload payload, IDialogCallBack callBack) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).showMessage(type, payload, callBack);
        }
    }

    @Override
    public void runOnUIThread(IRunEventCallBack callBack) {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).runOnUIThread(callBack);
        }
    }


    @Override
    public void disablePrimaryButton() {

    }


    public void setPrimaryButton(Button primaryButton) {
        if (mainActivity instanceof BaseActivity) {
            ((BaseActivity) mainActivity).setPrimaryButton(primaryButton);
        }
    }


    @Override
    public void enablePrimaryButton() {
        if (mainActivity instanceof IBaseViewUtilities) {
            ((IBaseViewUtilities) mainActivity).enablePrimaryButton();
        }
    }

    @Override
    public boolean InternetConnection(View view) {
        if (!NetworkUtil.isInternetavailable(CarpoolingApplication.getAppContext())) {
            showSnackBar(view);

            return false;
        }

        return true;
    }


    public void showHideSoftKeyBoard(Boolean show) {
        if (mainActivity instanceof BaseActivity) {
            ((BaseActivity) mainActivity).showHideSoftKeyBoard(getDialog(), show);
        }
    }

    public void showSnackBar(View linearLayout) {
        snackbar = Snackbar
                .make(linearLayout, "No Connection", Snackbar.LENGTH_INDEFINITE).
                        setAction("Ok", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        }).setBackgroundTint(getResources().getColor(R.color.white))
                .setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        snackbar.show();
    }

}
