package com.linarc.carpooling.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class PrefUtils {

    private static final String TAG = "PrefUtils";


    public static final String RIDE_ID = "rideId";
    public static final String RIDER_ID = "riderid";
    public static final String USER_ROLE = "userrole";
    public static final String UID = "uid";
    public static final String USER_FULL_NAME = "userFullName";

    public static void saveUID(Context activity, String uid) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        sp.edit().putString(UID, uid).commit();
    }

    public static String getUID(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String id = sp.getString(UID, "");
        return id;
    }

    public static void saveUserFullName(Context activity, String rideId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        sp.edit().putString(USER_FULL_NAME, rideId).commit();
    }

    public static String getUserFullName(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String id = sp.getString(USER_FULL_NAME, "");
        return id;
    }

    public static void saveRideId(Context activity, String rideId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        sp.edit().putString(RIDE_ID, rideId).commit();
    }

    public static String getRideId(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String id = sp.getString(RIDE_ID, "");
        return id;
    }

    public static void saveRiderId(Context activity, String riderId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        sp.edit().putString(RIDER_ID, riderId).commit();
    }

    public static String getRiderId(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String id = sp.getString(RIDER_ID, "");
        return id;
    }

    public static void saveUserRole(Context activity, String userRole) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        sp.edit().putString(USER_ROLE, userRole).commit();
    }

    public static String getUserRole(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String id = sp.getString(USER_ROLE, "");
        return id;
    }







}
