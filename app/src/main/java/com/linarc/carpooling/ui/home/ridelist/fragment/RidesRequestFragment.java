package com.linarc.carpooling.ui.home.ridelist.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.home.ridelist.adapter.RequestRidesAdapter;
import com.linarc.carpooling.ui.home.rider.model.RidesModel;
import com.linarc.carpooling.ui.utils.PrefUtils;

import java.util.ArrayList;

public class RidesRequestFragment extends Fragment {

    RecyclerView recyclerView;
    View root;
    RequestRidesAdapter ridesStatusAdapter;
    FirebaseFirestore db;

    public static RidesRequestFragment newInstance() {
        return new RidesRequestFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.rides_request_fragment, container, false);
        recyclerView = (RecyclerView) root.findViewById(R.id.rv_ridesrequest);
        ridesStatusAdapter = new RequestRidesAdapter(getActivity(), new ArrayList<RidesModel>());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(ridesStatusAdapter);



        db = FirebaseFirestore.getInstance();

        onCallDataFromFirestore();


        return root;
    }

    private void onCallDataFromFirestore() {
        String tripId = PrefUtils.getRideId(getActivity());
        db.collection("rides").document(tripId).collection("riders")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.d("Failed", "Listen failed.", e);
                            return;
                        }

                        ArrayList<RidesModel> riders = new ArrayList<>();
                        for (QueryDocumentSnapshot doc : value) {
                            RidesModel rider = new RidesModel();
                            String status = doc.getData().get("status").toString();
                            String fullName = doc.getData().get("fullName").toString();
                            String rideId = doc.getId();

                            rider.setStatus(status);
                            rider.setFullName(fullName);
                            rider.setRideId(rideId);

                            if(status.equals("REQ")){
                                riders.add(rider);
                            }
                        }

                        ridesStatusAdapter.reinitialze(riders, db, tripId);
                    }
                });
    }


}