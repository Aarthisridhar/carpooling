package com.linarc.carpooling.ui.home.rider;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.linarc.carpooling.R;
import com.linarc.carpooling.databinding.ActivityRiderStatusBinding;
import com.linarc.carpooling.ui.selection.SelectionActivity;
import com.linarc.carpooling.ui.utils.PrefUtils;
import com.linarc.carpooling.ui.utils.base.BaseActivity;
import com.linarc.carpooling.ui.utils.base.IDialogCallBack;
import com.linarc.carpooling.ui.utils.base.MessagePayload;
import com.linarc.carpooling.ui.utils.base.MessageType;

import java.util.Random;

public class RiderStatusActivity extends BaseActivity {
    FirebaseFirestore db;
    ActivityRiderStatusBinding activityRiderStatusBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRiderStatusBinding = ActivityRiderStatusBinding.inflate(getLayoutInflater());
        setContentView(activityRiderStatusBinding.getRoot());

        db = FirebaseFirestore.getInstance();
        onGetOwnerProfile();
        listenToAvailableOwners();

        activityRiderStatusBinding.btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMessage(MessageType.DIALOG, new MessagePayload("Cancel Ride", "Trip will be closed, Are you Sure?", "No", "Yes"), new IDialogCallBack() {
                    @Override
                    public void positiveButtonClick() {
                        onCancelRides();
                    }

                    @Override
                    public void negativeButtonClick() {

                    }
                });


            }
        });


    }

    private void onCancelRides() {
        String tripId = PrefUtils.getRideId(RiderStatusActivity.this);
        String rideId = PrefUtils.getRiderId(RiderStatusActivity.this);

        DocumentReference riderRef = db.collection("rides")
                .document(tripId).collection("riders")
                .document(rideId);

        riderRef
                .update("status", "DRP")
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        PrefUtils.saveUserRole(RiderStatusActivity.this, "");
                        Intent i = new Intent(RiderStatusActivity.this, SelectionActivity.class);
                        startActivity(i);
                        finish();
                        Log.d("Success", "DocumentSnapshot successfully updated!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("Failed", "Error updating document", e);
                    }
                });
    }

    private void onGetOwnerProfile() {
        String tripId = PrefUtils.getRideId(RiderStatusActivity.this);


        DocumentReference docRef = db.collection("rides").document(tripId);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {

                        String userFullName = document.getData().get("userFullName").toString();
                        String endLocation = document.getData().get("endLocation").toString();
                        String capacity = document.getData().get("capacity").toString();
                        String price = document.getData().get("price").toString();
                        String carName = document.getData().get("carName").toString();
                        String startLocation = document.getData().get("startLocation").toString();

                        activityRiderStatusBinding.tvRidername.setText(userFullName);
                        activityRiderStatusBinding.tvCarname.setText(carName);
                        activityRiderStatusBinding.tvEndLocation.setText("End Destination: " + endLocation);
                        activityRiderStatusBinding.tvPrice.setText(RiderStatusActivity.this.getResources().getString(R.string.rs) + price);
                        Random rnd = new Random();
                        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                        TextDrawable drawable = TextDrawable.builder()
                                .buildRound(userFullName.substring(0, 1), color);
                        activityRiderStatusBinding.ivSensorName.setImageDrawable(drawable);
                        Log.d("TAG", "DocumentSnapshot data: " + document.getData());
                    } else {
                        Log.d("TAG", "No such document");
                    }
                } else {
                    Log.d("TAG", "get failed with ", task.getException());
                }
            }
        });
    }

    private void listenToAvailableOwners() {
        String tripId = PrefUtils.getRideId(RiderStatusActivity.this);
        String rideId = PrefUtils.getRiderId(RiderStatusActivity.this);


        final DocumentReference docRef = db.collection("rides").document(tripId)
                .collection("riders").document(rideId);
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("TAG", "Listen failed.", e);
                    return;
                }
                if (snapshot != null && snapshot.exists()) {
                    String status = snapshot.getData().get("status").toString();
                    if (status.equalsIgnoreCase("REQ")) {
                        activityRiderStatusBinding.rideStatus.setText("Pending");
                        activityRiderStatusBinding.tvStatus.setText("Status: Ride Requested");
                    } else if (status.equalsIgnoreCase("APR")) {
                        activityRiderStatusBinding.rideStatus.setText("Ride Confirmed");
                        activityRiderStatusBinding.tvStatus.setText("Status: Ride Confirmed");
                    } else if (status.equalsIgnoreCase("PIC")) {
                        activityRiderStatusBinding.rideStatus.setText("Ride Confirmed");
                        activityRiderStatusBinding.tvStatus.setText("Status: Picked Up");
                    } else if (status.equalsIgnoreCase("DRP")) {
                        PrefUtils.saveUserRole(RiderStatusActivity.this, "");
                        showMessage(MessageType.DIALOG, new MessagePayload("Ride Closed", "Your Ride has been Ended", "", "Ok"), new IDialogCallBack() {
                            @Override
                            public void positiveButtonClick() {

                                Intent i = new Intent(RiderStatusActivity.this, SelectionActivity.class);
                                startActivity(i);
                                finish();
                                Log.d("Success", "DocumentSnapshot successfully updated!");

                            }

                            @Override
                            public void negativeButtonClick() {

                            }
                        });
                    } else if(status.equalsIgnoreCase("REJ")){
                        PrefUtils.saveUserRole(RiderStatusActivity.this, "");
                        showMessage(MessageType.DIALOG, new MessagePayload("Ride Rejected", "Your Ride has been Cancelled.", "", "Ok"), new IDialogCallBack() {
                            @Override
                            public void positiveButtonClick() {

                                Intent i = new Intent(RiderStatusActivity.this, SelectionActivity.class);
                                startActivity(i);
                                finish();
                                Log.d("Success", "DocumentSnapshot successfully updated!");

                            }

                            @Override
                            public void negativeButtonClick() {

                            }
                        });
                    }
                    Log.d("TAG", "Current data: " + snapshot.getData());
                } else {
                    Log.d("TAG", "Current data: null");
                }
            }
        });
    }
}