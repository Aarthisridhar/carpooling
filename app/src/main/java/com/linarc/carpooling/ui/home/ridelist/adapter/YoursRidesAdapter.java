package com.linarc.carpooling.ui.home.ridelist.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.linarc.carpooling.R;
import com.linarc.carpooling.ui.home.rider.model.RidesModel;
import com.linarc.carpooling.ui.utils.PrefUtils;

import java.util.List;
import java.util.Random;


public class YoursRidesAdapter extends RecyclerView.Adapter<YoursRidesAdapter.MyViewHolder> {
    private Context context;
    private List<RidesModel> riderList;
    FirebaseFirestore db;
    public String tripId;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView riderName, status;
        public Button startTrip, stopTrip;
        ImageView profileImage;

        public MyViewHolder(View view) {
            super(view);
            riderName = view.findViewById(R.id.tv_ridername);
            status = view.findViewById(R.id.tv_status);
            startTrip = view.findViewById(R.id.btn_accept);
            stopTrip = view.findViewById(R.id.btn_stop_ride);
            profileImage = view.findViewById(R.id.iv_SensorName);
        }
    }


    public YoursRidesAdapter(Context context, List<RidesModel> riderList) {
        this.context = context;
        this.riderList = riderList;
    }

    public void reinitialze(List<RidesModel> riderList, FirebaseFirestore db, String tripId) {
        this.db = db;
        this.tripId = tripId;
        this.riderList = riderList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_yours_rides, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final RidesModel ridesModel = riderList.get(position);
        String status = ridesModel.getStatus();

        holder.riderName.setText(ridesModel.getFullName());

        if(ridesModel.getStatus().equalsIgnoreCase("APR"))
        {
            holder.status.setText("Status: Way to PickUp");
        }else
        if(ridesModel.getStatus().equalsIgnoreCase("PIC"))
        {
            holder.status.setText("Status: Picked");
        }
       // holder.status.setText(ridesModel.getStatus());

        if(status.equals("APR")){
            holder.startTrip.setVisibility(View.VISIBLE);
            holder.stopTrip.setVisibility(View.GONE);
        }else if(status.equals("PIC")){
            holder.startTrip.setVisibility(View.GONE);
            holder.stopTrip.setVisibility(View.VISIBLE);
        }

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));

     if(!ridesModel.getFullName().isEmpty())
     {
         TextDrawable drawable = TextDrawable.builder()
                 .buildRound(ridesModel.getFullName().substring(0, 1), color);
         holder.profileImage.setImageDrawable(drawable);
     }


        holder.startTrip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                DocumentReference riderRef = db.collection("rides").document(tripId).collection("riders").document(ridesModel.getRideId());

                riderRef
                        .update("status", "PIC")
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("Success", "DocumentSnapshot successfully updated!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("Failed", "Error updating document", e);
                            }
                        });
            }
        });

        holder.stopTrip.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                DocumentReference riderRef = db.collection("rides").document(tripId).collection("riders").document(ridesModel.getRideId());

                riderRef
                        .update("status", "DRP")
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                PrefUtils.saveUserRole(context,"");
                                Log.d("Success", "DocumentSnapshot successfully updated!");
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("Failed", "Error updating document", e);
                            }
                        });
            }
        });
    }

    @Override
    public int getItemCount() {
        return riderList.size();
    }


}

   
