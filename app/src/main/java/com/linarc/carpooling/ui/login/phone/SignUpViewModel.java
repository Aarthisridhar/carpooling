package com.linarc.carpooling.ui.login.phone;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.auth.FirebaseUser;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.linarc.carpooling.ui.login.AuthAppRepository;
import com.linarc.carpooling.ui.utils.ValidationUtils;

public class SignUpViewModel extends AndroidViewModel {
    PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    Phonenumber.PhoneNumber availablePhoneNumber;
    public ObservableField<String> etphonenumber;
    public ObservableField<String> etfullname;
    public ObservableField<String> etpassword;
    public ObservableField<String> etmailid;
    public MutableLiveData<Boolean> progressdialog;
    public MutableLiveData<Boolean> onNextScreen;
    public MutableLiveData<String> showErrormsg;
    public MutableLiveData<String> showError;
    private AuthAppRepository authAppRepository;
    private MutableLiveData<FirebaseUser> userLiveData;
    private MutableLiveData<Boolean> processdialog;

    public SignUpViewModel(@NonNull Application application) {
        super(application);
        etphonenumber = new ObservableField<String>("");
        etfullname = new ObservableField<String>("");
        etpassword = new ObservableField<String>("");
        etmailid = new ObservableField<String>("");
        showError = new MutableLiveData<>("");

        progressdialog = new MutableLiveData<>();
        showErrormsg = new MutableLiveData<>();
        onNextScreen = new MutableLiveData<>();
        authAppRepository = new AuthAppRepository(application);
        userLiveData = authAppRepository.getUserLiveData();
        progressdialog = authAppRepository.getProgressdialog();
    }

    public MutableLiveData<Boolean>getProgressdialog()
    {
        return progressdialog;
    }

    public void onphonenumberchanged(CharSequence s, int start, int before, int count) {
        showErrormsg.setValue("");
    }

    public void onValidatephoneNumber() {


        if (etphonenumber.get().isEmpty()) {

            showErrormsg.setValue("Enter Valid Mobile Number");
            return;
        }


        String countryCode = "IN";
        try {

            availablePhoneNumber = phoneUtil.parse(etphonenumber.get(), countryCode);
        } catch (NumberParseException ex) {
            System.err.println("NumberParseException was thrown: " + ex.toString());

        }


        boolean isValid = phoneUtil.isValidNumber(availablePhoneNumber);


        if (!isValid) {
            showErrormsg.setValue("Enter Valid mobileNumber");
        } else {


            SharedPreferences pref = getApplication().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = pref.edit();

            editor.putString("Country", "IN");
            editor.putString("number", etphonenumber.get());
            editor.commit();
            showErrormsg.setValue("");
            onNextScreen.setValue(true);


        }

    }

    public void onSignUp() {

        if (etfullname.get().isEmpty()) {
            showError.postValue("Please Enter Full Name");
            return;
        }
        if (etmailid.get().isEmpty()) {
            showError.postValue("Please Enter vaild Mail id");
            return;
        }
         if (etpassword.get().isEmpty()) {
                showError.postValue("Please Enter Password");
                return;
        }
       /* if (etphonenumber.get().isEmpty() || ValidationUtils.isValidMobile("91"+etphonenumber.get())) {
            showError.postValue("Please Enter vaild mobile number");
            return;
        }*/

      //  processdialog.postValue(true);

        register(etmailid.get().trim(), etpassword.get(), etfullname.get().trim());




    }

    public void login(String email, String password) {
        authAppRepository.login(email, password);
    }

    public void register(String email, String password, String fullName) {
        authAppRepository.register(email, password, fullName);
    }

    public MutableLiveData<FirebaseUser> getUserLiveData() {
        return userLiveData;
    }

}
