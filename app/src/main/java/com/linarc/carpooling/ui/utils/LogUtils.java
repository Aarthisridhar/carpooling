package com.linarc.carpooling.ui.utils;


import android.util.Log;

import androidx.annotation.NonNull;

import com.linarc.carpooling.BuildConfig;

public class LogUtils {

    public static final String TEXT = "text";
    public static final String TAG_TAIL = "Tail";

    public static void d(@NonNull String tag, @NonNull String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }

    public static void i(@NonNull String tag, @NonNull String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, msg);
        }
    }

    public static void e(@NonNull String tag, @NonNull String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg);
        }
    }

    public static void v(@NonNull String tag, @NonNull String msg) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, msg);
        }
    }

    public static void w(@NonNull String tag, @NonNull String msg) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, msg);
        }
    }
}
