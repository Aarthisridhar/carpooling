package com.linarc.carpooling.ui.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.linarc.carpooling.R;


public class AnimationUtil {

    public static void slideUp(Context context, View view){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up_to_origin_slow);
        view.startAnimation(animation);
    }

    public static void slideDown(Context context, View view){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_down_to_origin_slow);
        view.startAnimation(animation);
    }
    public static void slidedownfast(Context context, View view){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.up_down);
        view.startAnimation(animation);
    }

    public static void slideFromLeft(Context context, View view){
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_left_to_right_slow);
        view.startAnimation(animation);
    }

}
