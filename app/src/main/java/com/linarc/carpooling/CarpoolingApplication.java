package com.linarc.carpooling;

import android.app.Application;
import android.content.Context;

public class CarpoolingApplication extends Application {
    private static Context mContext;

    @Override
    protected void attachBaseContext(Context base) {

        super.attachBaseContext(base);


    }

    @Override
    public void onCreate() {
        super.onCreate();


        mContext = getApplicationContext();


    }

    public static CarpoolingApplication get(Context context) {
        return (CarpoolingApplication) context.getApplicationContext();
    }


    public static Context getAppContext() {
        return mContext;
    }


}
