# CarPooling


## Application Architecture

High level Diagram of the app can be accessed from [here](https://lucid.app/lucidchart/419732fd-3f7f-41ab-a98e-4e0af4d3053b/edit?viewport_loc=104%2C299%2C1707%2C821%2C0_0&invitationId=inv_c097f7ca-6b74-4c3d-ab86-84d310c127f0#)

## Application Build

You can find the App APK [here](https://drive.google.com/file/d/1BCTElzo4d7MYxP0VV8K57ZCPbyFFGMmo/view?usp=sharing)

## Steps to run the code

- You need to have Android Studio installed in your system.
- Need to clone the code, below is the command to clone the code.
```
git clone https://gitlab.com/Aarthisridhar/carpooling.git
```
- After Cloning, go to ``` build.gradle ``` file and sync your gradle.
- Connect your Physical Android device to your system and enable USB Debugging in the mobile.
- Run the Application 
